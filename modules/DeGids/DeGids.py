import logging

import gzip
import json
import os
import pickle
import re
import string
import unidecode

from pynlpl.formats import folia

logger = logging.getLogger('gensim.corpora.foliacorpus')


class DeGids:
    """
    Object for reading and storing Nederlabdumps (FoLiA).
    """

    def __init__(self, metafile, corpuspath=None, debug=False,
                 gzipped=True):
        """
        """
        self.meta = dict()
        self.authors = dict()

        self.debug = debug
        self.gzipped = gzipped

        if self.debug:
            logger.info("Debug on: only analysing 2 files")

        if corpuspath:
            self.corpus = corpuspath
        else:
            self.corpus = os.path.dirname(metafile)

        if os.path.isfile(metafile):  # in case of one file (.json)
            self._loadfile(metafile)
        elif os.path.isdir(metafile):  # in case of multiple files (.json)
            for file in [f for f in os.listdir(metafile)
                         if f.endswith(".json")]:
                file = os.path.join(metafile, file)
                self._loadfile(file)
        else:
            raise FileNotFoundError

    def _loadfile(self, metafile):
        """
        Helper function to load metafile to class.
        :param metafile: filepath to metafile
        :return: None
        """

        with open(metafile, 'r', encoding='utf-8') as jsonfile:
            file = json.load(jsonfile)

            corpusfiles = os.listdir(self.corpus)

            for article in file['documents']:
                if article['NLCore_NLIdentification_versionID'] + ".xml.gz" not in corpusfiles and article['NLCore_NLIdentification_versionID'] + ".xml" not in corpusfiles:
                    continue
                else:
                    ID = article['NLCore_NLIdentification_sourceRef'][0]

                    self.meta[ID] = {
                        'url': article['NLCore_NLIdentification_sourceUrl'],
                        'versionID': article['NLCore_NLIdentification_versionID'],
                        'pages': (article['NLDependentTitle_startPage'], article['NLDependentTitle_endPage']),
                        'title': article['NLDependentTitle_title'],
                        'volumename': article['NLTitle_title'],
                        'year': article['NLTitle_yearOfPublicationMin']
                    }

                    if 'auteursgegevens' in article.keys():
                        authors = article['auteursgegevens']['documents']
                        self.meta[ID]['authors'] = []

                        for author in authors:
                            authorid = author['NLCore_NLIdentification_nederlabID']
                            self.meta[ID]['authors'].append(authorid)

                            self._addauthor(author)

    def _addauthor(self, author):
        """
        Helper function to add authors to meta in class object.
        :param author: dict() from metafile
        :return: None
        """
        authorid = author['NLCore_NLIdentification_nederlabID']

        name = author['NLPerson_NLPersonName_preferredFullName']
        years = (author.get('NLPerson_yearOfBirthLabel'),
                 author.get('NLPerson_yearOfDeathLabel'))

        self.authors[authorid] = {
            'name': name,
            'years': years
        }

    def _stopwords(self, filepath=None, punctuation=True):
        """
        Return or load and return the stoplist file as list.
        :param filepath:
        :return:
        """
        if 'stopwords' in vars(self):
            return self.stopwords
        elif filepath:
            with open(filepath, encoding='utf-8') as stoplistfile:
                stopwords = stoplistfile.read().lower().split('\n')[:-1]

                if punctuation:
                    stopwords += [char for char in string.punctuation]

                self.stopwords = stopwords

                return self.stopwords

    def id2file(self, id):
        """return filename for fileid"""
        if self.gzipped:
            return self.meta[id]['versionID'] + '.xml.gz'
        else:
            return self.meta[id]['versionID'] + '.xml'

    def file2id(self, filename):
        """return fileid for filename"""

        if filename.endswith(".xml"):
            filename = filename[:-4]
        elif filename.endswith(".xml.gz"):
            filename = filename[:-7]

        for ID in self.meta:
            if self.meta[ID]['versionID'] == filename:
                return ID

    def select(self, type='volume', years=(1837, 1936), output='ids', sort_output=True):
        """
        Select volume ids by selection criteria (years). Outputs list
        of ids or filenames.

        :param type: volume
        :param years: start and end year of search (inclusive)
        :param output: 'ids' for a list of ids, 'filenames' for a list
        of filenames pointing to the data files for each volume and 'objects'
        for a list of Document objects.
        :return: list of ids or filenames
        """

        startyear, endyear = years
        yearrange = range(startyear, endyear + 1)

        if output == 'ids':
            list_of_ids = [
                id for id in self.meta if self.meta[id]['year'] in yearrange]
            list_of_ids = sorted(
                list_of_ids, key=lambda x: (self.meta[x]['year'], x))

            selection = list_of_ids

        elif output == 'objects':
            list_of_ids = [
                id for id in self.meta if self.meta[id]['year'] in yearrange]
            list_of_ids = sorted(
                list_of_ids, key=lambda x: (self.meta[x]['year'], x))
            list_of_objects = [
                Document(id, self.meta[id], self.corpus, gzipped=self.gzipped) for id in list_of_ids]

            selection = list_of_objects

        elif output == 'filenames':
            if self.gzipped:
                list_of_filenames = [self.meta[id]['versionID'] +
                                     ".xml.gz" for id in self.meta
                                     if self.meta[id]['year'] in yearrange]
            else:
                list_of_filenames = [self.meta[id]['versionID'] +
                                     ".xml" for id in self.meta
                                     if self.meta[id]['year'] in yearrange]
            if sort_output:
                selection = sorted(list_of_filenames,
                                   key=lambda x: self.file2id(x))
            else:
                selection = list_of_filenames

        if self.debug:
            return selection[0:2]
        else:
            return selection

    def get_document(self, iddoc):
        """
        Process file
        :param id:
        :return: pynlpl.formats.folia.Document
        """

        if self.gzipped:
            filename = self.meta[iddoc]['versionID'] + '.xml.gz'
            filepath = os.path.join(self.corpus, filename)

            try:
                with gzip.open(filepath) as file:
                    document = folia.Document(string=file.read())

                return document

            except:
                print("No document found for id {iddoc}".format(iddoc=iddoc))
                print(filepath)
                return

        else:
            filename = self.meta[iddoc]['versionID'] + '.xml'
            filepath = os.path.join(self.corpus, filename)

            with open(filepath) as file:
                document = folia.Document(string=file.read())

                return document

    def id2author(self, id, output='name'):
        """
        Return the authors name (string), his/her year (tuple of strings)
        of birth and death or both (dict). The
        years are returned as string, since the field allows for incertainties
        such as '?(20ste eeuw)'.

        :param id: authors id as encoded in the meta file
        :param output: type of output, either 'name', 'years' or None (return all).
        :return:

        Example name:
        'Jérome Alexandre Sillem'

        Example years:
        ('?(20ste eeuw)', '1980')

        Example full:
        {'name': 'Jérome Alexandre Sillem', 'years': ('1840', '1912')}

        """

        author = self.authors[id]

        if output == 'name':
            return author['name']
        elif output == 'years':
            return author["years"]
        else:
            return author


class FoliaCorpus(DeGids):
    """
    For usage in gensim. Mostly copied from the
    gensim.corpora.textcorpus module.
    """

    def __init__(self, corpuspath=None, metafiles=None,
                 dictionary=None, years=(1837, 1936), lemma=True, 
                 add_entities=False, remove_stopwords=True, stopwordsfile="",
                 remove_accents=True, remove_nonalpha=True,
                 remove_punctuation=True, remove_hyphenation=True,
                 remove_apostrophe=True, transform_lowercase=True, **kwargs):

        self.corpuspath = corpuspath
        self.metafiles = metafiles
        self.lemma = lemma
        self.add_entities = add_entities
        self.years = years
        self.remove_stopwords = remove_stopwords
        self.stopwordsfile = stopwordsfile
        self.remove_accents = remove_accents
        self.remove_nonalpha = remove_nonalpha
        self.remove_punctuation = remove_punctuation
        self.remove_hyphenation = remove_hyphenation
        self.transform_lowercase = transform_lowercase
        self.remove_apostrophe = remove_apostrophe

        self.length = None
        self.dictionary = dictionary

        self.tfs = dict()

        if self.metafiles and self.corpuspath:
            DeGids.__init__(self, self.metafiles, self.corpuspath, **kwargs)
        self.init_dictionary(dictionary)

        self.statistics = self._get_statistics()

    def init_dictionary(self, dictionary):
        """If `dictionary` is None, initialize to an empty Dictionary,
        and then if there is an `input` for the corpus, add all documents
        from that `input`. If the `dictionary` is already initialized,
        simply set it as the corpus's `dictionary`.
        """

        from gensim.corpora.dictionary import Dictionary

        self.dictionary = dictionary if dictionary else Dictionary()
        if self.corpuspath is not None:
            if dictionary is None:
                logger.info("Initializing dictionary")

                # update the dictionary with every document and create an entry
                # for the token frequency.
                for document in self.get_texts():
                    for (tokid, freq) in self.dictionary.doc2bow(document, allow_update=True):
                        self.tfs[self.dictionary[tokid]] = self.tfs.get(self.dictionary[tokid], 0) + freq

                self.normalize()

            else:
                logger.info(
                    "Input stream provided but dictionary already initialized")
        else:
            logger.warning(
                "No input document stream provided; assuming dictionary will be \
                 initialized some other way.")

    def get_texts(self, filtering=True):
        """Iterate over the collection, yielding one document at a time.
        A document is a sequence of words (strings) that can be fed into
        `Dictionary.doc2bow`. Each document will be fed through
        `preprocess_text`. That method should be overridden to provide
        different preprocessing steps. This method will need to be
        overridden if the metadata you'd like to yield differs from
        the line number.

        Returns:
            generator of lists of tokens (strings); each list corresponds to a
            preprocessed document from the corpus `input`.
        """

        logger.info(
            "Quering texts from {}-{}".format(self.years[0], self.years[1]))

        for filename in super(FoliaCorpus, self).select(type='volume',
                                                        years=self.years,
                                                        output='filenames'):
            tokens = self.streamreader(filename)

            if filtering:
                tokens = self.preprocess(tokens)

            # add some statistics to the meta data
            self.meta[self.file2id(filename)]["length"] = len(tokens)

            yield tokens

    def streamreader(self, filename):
        """
        Instead of loading the entire document into memory,
        only return what we ask for. This only works for ungzipped documents.

        Possible streams:
            * folia.Word
            * folia.Lemma
        """
        docid = self.file2id(filename)
        logger.info("Reading {docid} ({year}) {title}".format(docid=docid,
                                                              year=self.meta[docid]['year'],
                                                              title=self.meta[docid]['title']))

        filepath = os.path.join(self.corpuspath, filename)

        if self.remove_punctuation:
            wcls = "PUNCTUATION"
        else:
            wcls = None

        if self.add_entities:
            #  construct a reader for memory efficient loading
            foliareader = folia.Reader(filepath, folia.Sentence)
            results = []

            for sentence in foliareader:
                
                # query the words in the sentence
                words = [i for i in sentence if type(i) == folia.Word]
                
                # then query the entities
                entities = [e for e in list(sentence.select(folia.EntitiesLayer))[0]]
                
                for e in entities:
                    wids = e.wrefs()
                    
                    # replace the corresponding words with an entity
                    words[words.index(wids[0]):words.index(wids[-1])+1] = [e]
                    
                for i in words:
                    if type(i) == folia.Word and i.cls != wcls:
                        if self.lemma:
                            results.append(i.lemma())
                        else:
                            results.append(i.text())
                    elif type(i) == folia.Entity:
                        if self.remove_punctuation:
                            # also lemmatise the entities, why not
                            if self.lemma:
                                results.append('_'.join([w.lemma() for w in i if w.lemma() not in string.punctuation]))
                            else:
                                results.append('_'.join([w.text() for w in i if w.text() not in string.punctuation]))
                        else:
                            # also lemmatise the entities, why not
                            if self.lemma:
                                results.append('_'.join([w.lemma() for w in i]))
                            else:
                                results.append('_'.join([w.text() for w in i]))
            return results

        else:
            #  construct a reader for memory efficient loading
            foliareader = folia.Reader(filepath, folia.Word)

            if self.lemma:
                result = [w.lemma() for w in foliareader if w.cls != wcls]
            else:
                result = [w.text() for w in foliareader if w.cls != wcls]

        return result

    def preprocess(self, result):
        """
        This function modifies the requested token in place. If requested, it
        returns the iterable of tokens (1) lowercase, (2) unaccented and
        (3) without hypens.

        Returns: iterable of tokens
        """

        # Some filtering before tokens are served

        # if requested return the string lowercase
        if self.transform_lowercase:
            result = [r.lower() for r in result]

        # if requested return the string without any punctuation in it
        # which is not filtered by the tokeniser. 
        if self.remove_punctuation:
            result = [re.sub('[!()+,.:;?]', '', r) for r in result]

        # if requested return the string without diacritics
        if self.remove_accents:
            result = [unidecode.unidecode(r) for r in result]

        # if requested return tokens without hyphen
        if self.remove_hyphenation:
            result = [re.sub('-', '', r) for r in result]

        # if requested return tokens without apostrophe
        if self.remove_apostrophe:
            result = [re.sub("(l')|(d')|(s')|(c')|(n')|(j')|(m')|(qu')|(t')|(g')|('s)|('$)", "", r) for r in result]
            #result = [r for r in result]
        return result

    def normalize(self):
        """
        Normalization step for the entire corpus. Since text is retreived by
        issuing the dictionary's doc2bow function, deleting the bad_ids from
        the dictioinary will remove them from any call. 
        """

        bad_ids = []

        # remove non-alphanumeric
        if self.remove_nonalpha:
            bad_ids += [id for id in self.dictionary if self.dictionary[id]
                        in '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~']
            bad_ids += [id for id in self.dictionary if self.dictionary[id].isnumeric()]

        # remove stopwords
        if self.remove_stopwords:
            with open(self.stopwordsfile, encoding='utf-8') as stopwordsfile:
                stopwords = stopwordsfile.read().split('\n')

            bad_ids += [self.dictionary.token2id[stopword]
                        for stopword in stopwords if stopword in self.dictionary.token2id]

        # remove the ids in bad_ids from the dictionary and the term frequencies
        self.tfs = {token: freq for token, freq in self.tfs.items(
        ) if self.dictionary.token2id[token] not in bad_ids}
        
        self.dictionary.filter_tokens(bad_ids=bad_ids)

    def filter(self, min_dfs=1, max_dfs=1.0, min_tfs=1, remove_top_terms=0):
        """
        """

        bad_ids = []

        max_dfs = len(self) * max_dfs
        # remove_top_frequent = 

        # minimum dfs
        bad_ids += [tokenid for tokenid in self.dictionary.dfs if self.dictionary.dfs[tokenid] < min_dfs]
        bad_ids += [tokenid for tokenid in self.dictionary.dfs if self.dictionary.dfs[tokenid] > max_dfs]


        # mimimum tfs
        bad_ids += [self.dictionary.token2id[token] for token in self.tfs if self.tfs[token] < min_tfs]

        # remove the ids in bad_ids from the dictionary and the term frequencies
        self.tfs = {token: freq for token, freq in self.tfs.items(
        ) if self.dictionary.token2id[token] not in bad_ids}
        
        self.dictionary.filter_tokens(bad_ids=bad_ids)

        # new statistics needed
        self.statistics = self._get_statistics()


    def _get_statistics(self):
        """
        Give statistics on the amount of tokens, the vocabulary size and the
        amount of documents in the current corpus. 
        """
        
        stats = {
            "tokens": sum(self.tfs[self.dictionary[tokenid]] for tokenid in self.dictionary),
            "vocabulary size": len(self.dictionary),
            "documents:": len(self)
        }
        
        return stats

    def __iter__(self):
        """
        The function that defines a corpus.
        Iterating over the corpus must yield sparse vectors,
        one for each document.
        """

        for text in self.get_texts():
            yield self.dictionary.doc2bow(text, allow_update=False)

    def __len__(self):
        if self.length is None:
            # cache the corpus length
            self.length = sum(1 for _ in super(FoliaCorpus, self).select(
                type='volume', years=self.years, output='ids'))
        return self.length

    def serialize(self, corpusname, filepath=''):
        """
        Save the corpus as processed, altered and filtered in a faster format
        for the topic modeling algorithm. 

        :param corpusname: Name of the file (e.g. years that are used)
        :param filepath: Store in a different location than cwd. 
        """

        from gensim.corpora import MmCorpus

        with open(os.path.join(filepath, corpusname + '.foliacorpus'), 'wb') as pickleout:
            pickle.dump(self, pickleout)

        MmCorpus.serialize(os.path.join(filepath, corpusname + '.mmcorpus'), self)


class Document:
    """
    Class to store an individual corpus file
    """

    def __init__(self, id, meta, corpus, gzipped=True):
        """

        :param id:
        :param meta:
        :param corpus:
        """
        self.id = id
        self.corpus = corpus

        self.gzipped = gzipped

        #  add meta vars as attributes
        for k, v in meta.items():
            setattr(self, k, v)

    def document(self):
        """
        Process file and return FoLiA document.
        :param id:
        :return: pynlpl.formats.folia.Document
        """
        logger.info("Reading {} {}".format(self.year, self.title))

        if '_document' in vars(self):
            return self._document
        else:
            if self.gzipped:
                filename = self.versionID + '.xml.gz'
                filepath = os.path.join(self.corpus, filename)

                try:
                    with gzip.open(filepath) as file:
                        document = folia.Document(string=file.read())

                    self._document = document
                    return document

                except:
                    print("No document found for id {iddoc}".format(
                        iddoc=self.versionID))
                    return
            else:
                filename = self.versionID + '.xml'
                filepath = os.path.join(self.corpus, filename)

                with open(filepath) as file:
                    document = folia.Document(string=file.read())

                self._document = document
                return document

    def text(self, removestopwords=False, lemmas=False):
        customstopwords = ['den', 'zoo']

        if removestopwords:

            stopwords = super()._stopwords()

            if lemmas:
                words = [w for w in self.lemmas() if w not in stopwords]
            else:
                words = [w for w in self.words() if w not in stopwords]
            text = " ".join(words)
        else:
            if lemmas:
                text = " ".join(self.lemmas())
            else:
                document = self.document()
                text = document.text()

        return text

    def words(self, lower=False):
        """
        Return a list of words from the document.
        This function returns the words from the document in the order
        they appear in the text.

        :return:
        """
        if self.document():
            if lower:
                return [w.text().lower() for w in self._document.words()]
            else:
                return [w.text() for w in self._document.words()]
        else:
            return

    def lemmas(self, lower=False):
        """
        For every word in the document, return its lemma.
        This function returns a list of lemmas of the words that are used
        in the document. It returns them in the order they appear in the text.

        :return: list
        """
        if self.document():
            if lower:
                return [w.lemma().lower() for w in self._document.words()]
            else:
                return [w.lemma() for w in self._document.words()]
        else:
            return

    def ngrams(self, n):
        """
        Return n-grams for given length. This function creates ngrams of
        a given length by quering the sentences"from the document and
        constructing the ngrams from the word collocations. The absolute
        and relative frequency are returned based on the document's content.

        :return: tuple
        """

        for sent in self._document.sentences():
            pass
