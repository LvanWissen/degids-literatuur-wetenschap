# DeGids
###### Can be used as a corpus reader for FoLiA exports (e.g. from Nederlab) or can be used as a corpus reader in Gensim (https://radimrehurek.com/gensim/).

## How to use

### As an ordinary corpus reader

```python
from DeGids.DeGids import DeGids

CORPUS = "data/degids_1837_1936/"
METAFILES = "data/metadata_auteursinformatie"

dg = DeGids(metafile=METAFILES, corpuspath=CORPUS)

selection_of_documents = dg.select(years=(1837, 1936), output='ids')

```

### In Gensim

```python
from DeGids.DeGids import FoliaCorpus

CORPUS = "data/degids_1837_1936/"
METAFILES = "data/metadata_auteursinformatie"

corpus = FoliaCorpus(input=CORPUS, metafiles=METAFILES)  # doesn't load the corpus into memory!

```
