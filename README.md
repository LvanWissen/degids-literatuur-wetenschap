# De Gids Literatuur en Wetenschap

![afbeelding](https://www.de-gids.nl/assets/images/gidslogo.png)

## Instructies

Alles is relatief leesbaar te vinden in een zogenaamde 'Notebook'. Zie: `degids_literatuur_en_wetenschap.ipnb'`. 

### Vereiste pakketten

### Corpus

Het corpus kan door zijn licentie niet meegeleverd worden in deze repository. Wel is het model van 300 topics beschikbaar, samen met de index van woorden. Dit is een (voor het programma voldoende) benadering/abstractie van de representatie van de inhoud van de echte artikelen. Het gehele corpus van deze periode is overigens vrij in te zien in de [DBNL](https://www.dbnl.org/auteurs/auteur.php?id=_gid001). 
